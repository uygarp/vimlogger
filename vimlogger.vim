if !has('python3')
    echo "Error: Required vim compiled with +python3"
    finish
endif

python3 << EOF

import os, sys
import importlib

_module_name = vim.eval("expand('<sfile>:t')")[:-4]
_module_path = os.path.abspath(vim.eval("expand('<sfile>:p:h')"))

#try:
sys.path.append(_module_path)
_module = importlib.import_module(_module_name)
_module = importlib.reload(_module)
_module.main()
sys.path.remove(_module_path)
#except:
    #print("Error occurred while running module '%s'..." % _module_name)

EOF
