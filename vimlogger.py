#!/bin/python

import vim
import os

def add_custom_cmd(cmd, func_name, nargs="0"):
    if nargs not in ["0", "1", "?", "*", "+"]:
        print("Error in nargs param!")
        return
    _cmd = """
        function! s:{0}(param)
            py3 import {2}
            py3 {2}.{1}(vim.eval("a:param"))
        endfunction
        command! -nargs={3} {0} call <SID>{0}(<args>)
        """.format(cmd, func_name, os.path.basename(__file__)[:-3], nargs)
    vim.command(_cmd)

def add_custom_key(key, func_name):
    _cmd = """
        function! {0}(param1, param2)
            py3 import {2}
            py3 {2}.{1}(vim.eval("a:param1"), vim.eval("a:param2"))
        endfunction
        nnoremap {3} :call {0}("{3}", "n")<CR>
        vnoremap {3} :call {0}("{3}", "v")<CR>
        """.format(func_name.capitalize(), func_name, os.path.basename(__file__)[:-3], key)
    vim.command(_cmd)
    #vim.command("vmap <expr> {0} {2}(\"{0}\", \"{1}\")".format(key, "v", func_name.capitalize()))

def rem_custom_key(key):
    vim.command("exe ':unmap {0}'".format(key))

"""
LOCAL FUNCTIONS
"""
paint_key = "<F9>"
clear_key = "<F8>"
color_dict = {"1":"105,black,hotpink,black",
              "2":"155,black,hotpink,black",
              "3":"225,black,hotpink,black",
              "4":"205,black,hotpink,black",
              "5":"black,white,hotpink,black",
              "6":"255,white,hotpink,black",
            }
str_dict = {"Updating":"0",
            "enabled":"1"
}

def enable_vlog(enabled_str):
    try:
        if int(enabled_str) not in [0, 1]:
            print("Error! Invalid parameter...")
            return
        enabled = int(enabled_str) == 1
    except:
        print("Error! Invalid parameter...")
        return

    #print("Applying makeup %d" % enabled)
    if enabled:
        for ii in range(1, len(color_dict) + 1):
            add_custom_key(paint_key + str(ii), "apply_color")
            add_custom_key(clear_key + str(ii), "clear_color")
    else:
        for ii in range(1, len(color_dict) + 1):
            rem_custom_key(paint_key + str(ii))
            rem_custom_key(clear_key + str(ii))

def get_selected_str_inline():
    buf = vim.current.buffer
    (lnum1, col1) = buf.mark('<')
    (lnum2, col2) = buf.mark('>')
    if lnum1 == lnum2:
        lines = vim.eval('getline({}, {})'.format(lnum1, lnum2))
        #lines = vim.eval("getline(\'.\')")
        lines[0] = lines[0][col1:col2 + 1]
        return "\n".join(lines)
    else:
        return ""

def get_str_under_cursor():
    return vim.eval("expand(\"<cword>\")")

def get_str(mode):
    line_buf = ""
    if mode == "v":
        line_buf = get_selected_str_inline()
    else:
        line_buf = get_str_under_cursor()
    return line_buf

def apply_color(key, mode):
    color_code = key[-1:]
    color = color_dict[color_code].replace(" ", "").split(",")    
    #str_to_paint = "Updating"
    str_to_paint = get_str(mode)
    
    vim.command("hi CustomColour{0} ctermbg={1} ctermfg={2} guibg={3} guifg={4}".format(color_code, color[0], color[1], color[2], color[3]))
    vim.command("call matchadd(\'CustomColour{0}\', \'{1}\')".format(color_code, str_to_paint))
    str_dict[str_to_paint] = color_code

def clear_color(key, mode):
    print("Clearing color..." + key[-1:])

def main():
    #vim.command("hi CustomPink ctermbg=205 guibg=hotpink guifg=black ctermfg=black")
    #vim.command("call matchadd('CustomPink', 'Updating')")
    #vim.command("hi CustomBlue ctermbg=105 guibg=hotpink guifg=black ctermfg=black")
    #vim.command("call matchadd('CustomBlue', 'Second')")
    add_custom_cmd("Vlog", "enable_vlog", "1")

#if __name__ == '__main__':
#    main()
